package main

import (
  "fmt"
  "bitbucket.org/matoski/string"
)

func main() {
  fmt.Printf(string.Reverse("hello, world\n"))
}
